import unittest
from io import StringIO
from unittest.mock import patch
from loop_100_times import run_loop_100_times

class TestLoop100Times(unittest.TestCase):
    @patch('sys.stdout', new_callable=StringIO)
    def test_run_loop_100_times(self, mock_stdout):
        run_loop_100_times()
        output = mock_stdout.getvalue()
        self.assertEqual(output.count("Iteration:"), 100)

if __name__ == '__main__':
    unittest.main()
