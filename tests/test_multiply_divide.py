import unittest
from multiply_divide import multiply_numbers, divide_numbers

class TestMultiplyDivide(unittest.TestCase):
    def test_multiply_numbers(self):
        result = multiply_numbers(2, 3)
        self.assertEqual(result, 6)

    def test_divide_numbers(self):
        result = divide_numbers(10, 2)
        self.assertEqual(result, 5)

    def test_divide_by_zero(self):
        with self.assertRaises(ZeroDivisionError):
            divide_numbers(10, 0)

if __name__ == '__main__':
    unittest.main()
