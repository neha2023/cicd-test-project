import unittest
from unittest.mock import patch
from print_random import print_random_number

class TestPrintRandom(unittest.TestCase):
    @patch('print_random.random.randint')
    def test_print_random_number(self, mock_randint):
        mock_randint.return_value = 42
        with patch('builtins.print') as mock_print:
            print_random_number()
            mock_print.assert_called_with("Random Number:", 42)

if __name__ == '__main__':
    unittest.main()
