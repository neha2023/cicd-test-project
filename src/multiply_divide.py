def multiply_numbers(a, b):
    return a * b

def divide_numbers(a, b):
    if b != 0:
        return a / b
    else:
        raise ZeroDivisionError("Cannot divide by zero.")
