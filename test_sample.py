import pytest
def func(x):
    return x+1   


def test_x1(record_property):
    assert func(4) == 2

def test_x2(record_property):
    assert func(4) == 4

def test_x3(record_property):
    assert func(4) == 6

